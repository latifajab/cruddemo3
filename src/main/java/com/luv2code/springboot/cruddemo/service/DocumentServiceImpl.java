package com.luv2code.springboot.cruddemo.service;

import com.luv2code.springboot.cruddemo.Exeception.AucunDocumentTrouveException;
import com.luv2code.springboot.cruddemo.Exeception.DocumentInexistantException;
import com.luv2code.springboot.cruddemo.Exeception.ListeDocumentsVideException;
import com.luv2code.springboot.cruddemo.dao.DocumentRepository;
import com.luv2code.springboot.cruddemo.dao.QDocumentRepo;
import com.luv2code.springboot.cruddemo.dto.DocumentDto;
import com.luv2code.springboot.cruddemo.dto.DocumentMapper;
import com.luv2code.springboot.cruddemo.entity.Document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

private DocumentRepository employeeRepository;
private QDocumentRepo qDocumentRepo;

@Autowired
    public DocumentServiceImpl(DocumentRepository employeeRepository, QDocumentRepo qDocumentRepo) {
        this.employeeRepository = employeeRepository;
        this.qDocumentRepo = qDocumentRepo;
    }

    @Autowired
    DocumentMapper documentMapper;


    @Override
    public List<Document> findAll() throws ListeDocumentsVideException {


        if( employeeRepository.findAll().isEmpty()){
            throw new ListeDocumentsVideException("list vide");
        };
        return employeeRepository.findAll();
    }

    @Override
    public Document findById(Integer theId) throws DocumentInexistantException {
Document document=employeeRepository.findById(theId).orElseThrow(()->new DocumentInexistantException("employee with this id deos not exist"));


return document ;
    }

    @Override
    public void save(Document employee) {
        employeeRepository.save(employee);
    }
   
    @Override
    public void deleteByIdd(int theId) {
        employeeRepository.deleteById(theId);
    }
    @Override
    public List<Document>findByTitle(String title) throws AucunDocumentTrouveException {


    ;if (employeeRepository.findNameByTitle(title)==null){

            throw new AucunDocumentTrouveException("pas de document");
        }
        return employeeRepository.findNameByTitle(title);
    }

    @Override
    public List<Document> findByTitleQueryDsl(String motCle) {



        return qDocumentRepo.findByTitleQueryDsl(motCle);
    }
    @Override
    public DocumentDto transfertDocToDto(Document document){
        return documentMapper.asDocumentDTO(document);

    }

    @Override
    public Document transfertDtoToDoc(DocumentDto documentDto) {
        return documentMapper.asDTODocument(documentDto);
    }



}
