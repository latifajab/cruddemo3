package com.luv2code.springboot.cruddemo.service;

import com.luv2code.springboot.cruddemo.Exeception.AucunDocumentTrouveException;
import com.luv2code.springboot.cruddemo.Exeception.DocumentInexistantException;
import com.luv2code.springboot.cruddemo.Exeception.ListeDocumentsVideException;
import com.luv2code.springboot.cruddemo.dto.DocumentDto;
import com.luv2code.springboot.cruddemo.entity.Document;

import java.util.List;

public interface DocumentService {
public List <Document> findAll() throws ListeDocumentsVideException;
public Document findById(Integer theId) throws DocumentInexistantException;
public void save(Document employee);
public void deleteByIdd(int theId);
    public List<Document> findByTitle(String title) throws AucunDocumentTrouveException;

    public List<Document> findByTitleQueryDsl(String motCle);

    public DocumentDto transfertDocToDto(Document document);
    public Document transfertDtoToDoc(DocumentDto documentDto);


}
