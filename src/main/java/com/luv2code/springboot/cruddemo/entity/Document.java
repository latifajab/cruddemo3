package com.luv2code.springboot.cruddemo.entity;

import lombok.Builder;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.Date;
@Builder
@Entity
@Table(name="document")
public class Document {
    //define fields
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="id")
    private Integer id;
    @Column(name="titre")
    @NotBlank(message = "Titre ne doit pas etre null")
    private String titre;
    @NotEmpty(message = "Please provide an author")
    @Column(name="author")
    private String author;
    @Column(name="date")

    @Past(message = "date is invalid ")
    private LocalDate date;

    //define constuctor


    public Document() {

    }

    public Document(int id, String titre, String author, LocalDate date) {

        this.id = id;
        this.titre = titre;
        this.author = author;
        this.date = date;
    }
    //define getter and setter

    public Document( String titre, String author, LocalDate date) {

        this.titre = titre;
        this.author = author;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getAuthor() {
        return author;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
