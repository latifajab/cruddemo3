package com.luv2code.springboot.cruddemo.Exeception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerAdvisor {


       /* @ExceptionHandler({
                    DocumentInexistantException.class,
                ListeDocumentsVideException.class,
                AucunDocumentTrouveException.class

        })*/


    @ExceptionHandler(DocumentInexistantException.class)
    public ResponseEntity<Object> handleListEmptyException(
            DocumentInexistantException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(AucunDocumentTrouveException.class)
    public ResponseEntity<Object> handleListEmptyException(
            AucunDocumentTrouveException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ListeDocumentsVideException.class)
    public ResponseEntity<Object> handleListEmptyException(
            ListeDocumentsVideException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }
    //MethodArgumentNotValidException
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleDateException(
            MethodArgumentNotValidException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }



//        public ResponseEntity<Object> handleRestControllerExceptions(
//                Exception exception,
//                WebRequest request
//        ) {
//            Map<String, Object> body = new HashMap<>();
//            body.put("error message", exception.getMessage());
//            return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
//        }

    }
