package com.luv2code.springboot.cruddemo.Exeception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ListeDocumentsVideException extends Exception {
    public ListeDocumentsVideException(String s){
        super(s);

    }
}
