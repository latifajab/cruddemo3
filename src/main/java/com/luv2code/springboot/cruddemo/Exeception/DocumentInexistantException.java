package com.luv2code.springboot.cruddemo.Exeception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DocumentInexistantException extends Exception {
    public DocumentInexistantException(String s){
        super(s);
    }

}
