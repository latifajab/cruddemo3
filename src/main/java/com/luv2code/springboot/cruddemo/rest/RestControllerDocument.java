package com.luv2code.springboot.cruddemo.rest;

import com.luv2code.springboot.cruddemo.Exeception.AucunDocumentTrouveException;
import com.luv2code.springboot.cruddemo.Exeception.DocumentInexistantException;
import com.luv2code.springboot.cruddemo.Exeception.ListeDocumentsVideException;
import com.luv2code.springboot.cruddemo.dto.DocumentDto;
import com.luv2code.springboot.cruddemo.entity.Document;
import com.luv2code.springboot.cruddemo.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RestControllerDocument {


@Autowired
        private DocumentService documentService;

        //inject emplyee dao

        //ewpose "/employees " and return list of employees

        @GetMapping("/document")
        public List<Document> findAll() throws ListeDocumentsVideException {

            return documentService.findAll();
        }
        //add  mapping for get /employees/{}
        @GetMapping("{documentId}")
        public Document getDocument(@PathVariable("documentId") Integer id) throws DocumentInexistantException {

           Document document=documentService.findById(id);


            return document;
        }
    @GetMapping(value = "/doc", params = "motCle")
    public List<Document> getDocumentByTitle(@RequestParam("motCle") String motCle ) throws AucunDocumentTrouveException {

          return   documentService.findByTitle(motCle);
        }
    @GetMapping(value = "/dsl", params = "motCle")
    public List<Document> getDocumentByTitleQueryDsl(@RequestParam("motCle") String motCle )  {

        return  documentService.findByTitleQueryDsl(motCle);
    }

      @PostMapping("/document")
        public  Document addEmployee(@Valid @RequestBody  Document document){

         //also just in case they pass an id we have to set ot to zero 0
            //force a safe of new item instead od update
        // document.setId(0);
           documentService.save(document);
           return document;

       }
    @PostMapping("/documentdto")
    public  Document addEmployee(@RequestBody DocumentDto documentDto){

        //also just in case they pass an id we have to set ot to zero 0
        //force a safe of new item instead od update
        // document.setId(0);


        return  documentService.transfertDtoToDoc(documentDto);

    }

       @GetMapping("/dto/{id}")
    DocumentDto transfetDtoToDoc(@PathVariable Integer id) throws DocumentInexistantException {
          Document  document=documentService.findById(id);

return documentService.transfertDocToDto(document);

       }
//        //add mapping for put /employees  -update existing employee
//
//        @PutMapping("/employeees")
//        public  Employee updateEmployee(@RequestBody Employee employee){
//            employeeService.save(employee);
//            return employee;
//        }
//        //delete employee /{employeeId}
//        @DeleteMapping("/employe/{emplId}")
//        public String deleteEmploye(@PathVariable int emplId){
//            Employee employee=employeeService.findById(emplId);
//            if(employee==null){
//
//                throw new RuntimeException("employee id is not found"+emplId);
//            }
//            employeeService.deleteByIdd(emplId);
//            return "emplyee deleted with id ="+ emplId;
//        }
//



    }
