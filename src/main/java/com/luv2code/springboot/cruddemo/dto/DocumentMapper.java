package com.luv2code.springboot.cruddemo.dto;

import com.luv2code.springboot.cruddemo.entity.Document;
import fr.xebia.extras.selma.IgnoreFields;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;


@Mapper(withIgnoreFields={"date","title"},withIoC = IoC.SPRING,
        withIgnoreMissing = IgnoreMissing.ALL)

public interface DocumentMapper {

    DocumentDto asDocumentDTO(Document source);
    Document asDTODocument(DocumentDto source);



}
