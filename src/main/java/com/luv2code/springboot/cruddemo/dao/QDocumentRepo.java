package com.luv2code.springboot.cruddemo.dao;

import com.luv2code.springboot.cruddemo.entity.Document;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface QDocumentRepo {

   List<Document> findByTitleQueryDsl(String motCle);
}
