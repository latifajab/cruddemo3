package com.luv2code.springboot.cruddemo.dao;

import com.luv2code.springboot.cruddemo.entity.Document;
import com.luv2code.springboot.cruddemo.entity.QDocument;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class QDocumentRepoImpl implements QDocumentRepo{

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Document> findByTitleQueryDsl(String motCle) {
        final JPAQuery<Document> query=new JPAQuery(em);
        final QDocument doc=QDocument.document;
        return query.from(doc).where(doc.titre.eq(motCle)).fetch();
        //.contains une partie
    }
}
