package com.luv2code.springboot.cruddemo;

import com.luv2code.springboot.cruddemo.dao.DocumentRepository;
import com.luv2code.springboot.cruddemo.dao.QDocumentRepo;
import com.luv2code.springboot.cruddemo.entity.Document;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
public class DocumentRepositoryDao {

@Autowired
    DocumentRepository documentRepository;
@Autowired
    QDocumentRepo qDocumentRepo;
//test for save document
    @Test
    public void saveDocumentTest(){

       // Document document=Document.builder().titre("spring").author("code").date(LocalDate.parse("2018-05-05")).build();
     Document document=new Document("spring" ,"code",LocalDate.parse("2018-05-05"));

        Document savedDocument=documentRepository.save(document);
assertNotNull(savedDocument.getId());

       Assertions.assertThat(savedDocument.getId()).isGreaterThan(0);


    }
    @Test
    public void getAllDocuments(){

        List<Document> documentExpected=new ArrayList<>();
        for(int i=0;i<17;i++){


            documentExpected.add(new Document("latifa","jabri",LocalDate.parse("2018-05-05")));
        }


        List<Document> documentGoted=documentRepository.findAll();
        for(int i=0;i<17;i++){}
       // assertEquals(documentExpected.get(i).getId(),documentGoted.get(i).getId());}
        assertNotNull(documentGoted);

    }
    @Test
    public void getDocumentById(){
        Document document=new Document(17,"spring" ,"code",LocalDate.parse("2018-05-05"));
documentRepository.save(document);
      Document documentById=documentRepository.findById(17).get();

        assertEquals(document.getId(),documentById.getId());
        assertEquals(document.getTitre(),documentById.getTitre());
        assertEquals(document.getDate(),documentById.getDate());
        assertEquals(document.getAuthor(),documentById.getAuthor());

    }
    @Test
    public void getDocumentByTitle(){



        List<Document> documentGoted=qDocumentRepo.findByTitleQueryDsl("spring");


        assertNotNull(documentGoted);

    }







}
